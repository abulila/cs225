#include "list.h"
#include <iostream>

int main() {

    /// Test reverse funtions
    // List<unsigned> list;
    // for (unsigned i = 1; i < 10; i++) {
    //     list.insertBack(i);
    // }
    //
    // std::cout << list << std::endl;
    // // list.reverse();
    // list.reverseNth(3);
    // std::cout << list << std::endl;

    /// Test waterfall
    List<int> list;
    for (int i = 1; i < 10; i++) {
        list.insertBack(i);
    }

    std::cout << list << std::endl;
    list.waterfall();
    std::cout << list << std::endl;

    list.sort();
    std::cout << list << std::endl;

    /// Test merge
    // List<int> list1;
    // for (int i = 0; i < 10; i += 2) {
    //     list1.insertBack(i);
    // }
    // List<int> list2;
    // for (int i = 1; i < 15; i += 3) {
    //     list2.insertBack(i);
    // }
    //
    // std::cout << list1 << std::endl;
    // std::cout << list2 << std::endl;
    // list1.mergewith(list2);
    // std::cout << list1 << std::endl;
    // std::cout << list2 << std::endl;

    return 0;
}
