/**
 * @file list.cpp
 * Doubly Linked List (MP 3).
 */

#include <iostream>
#include <stack>

/**
 * Destroys the current List. This function should ensure that
 * memory does not leak on destruction of a list.
 */
template <class T>
List<T>::~List() {
    /// @TODO Graded in MP3.1
    clear();
}

/**
 * Destroys all dynamically allocated memory associated with the current
 * List class.
 */
template <class T>
void List<T>::clear() {
    /// @TODO Graded in MP3.1

    if (length_ == 0) {
        return;
    }

    // start at the second to last element
    // and delete forward until we reach
    // the head (i.e. ->prev == NULL)
    ListNode* temp = tail_->prev;
    while (temp != NULL) {
        delete temp->next;
        temp = temp->prev;
        length_ --;
    }
    // delete the head
    delete head_;
}

/**
 * Inserts a new node at the front of the List.
 * This function **SHOULD** create a new ListNode.
 *
 * @param ndata The data to be inserted.
 */
template <class T>
void List<T>::insertFront(T const & ndata) {
    /// @TODO Graded in MP3.1
    ListNode* newNode = new ListNode(ndata);  // allocate the new node

    if (length_ == 0) {                       // if the list is empty, set the new
        head_ = tail_ = newNode;              // node as both the head and the tail
    }
    else {
        newNode->next = head_;                // link the new node to the old head
        head_->prev = newNode;                // and link the head back to the new node
        head_ = newNode;                      // then move the head pointer to the new head
    }
    length_ ++;
}

/**
 * Inserts a new node at the back of the List.
 * This function **SHOULD** create a new ListNode.
 *
 * @param ndata The data to be inserted.
 */
template <class T>
void List<T>::insertBack(const T & ndata) {
    /// @TODO Graded in MP3.1
    ListNode* newNode = new ListNode(ndata);  // allocate the new node

    if (length_ == 0) {                       // if the list is empty, set the new
        head_ = tail_ = newNode;              // node as both the head and the tail
    }
    else {
        newNode->prev = tail_;                // link the new node to the old tail
        tail_->next = newNode;                // and link the tail back to the new node
        tail_ = newNode;                      // then move the tail pointer to the new head
    }
    length_ ++;
}

/**
 * Reverses the current List.
 */
template <class T>
void List<T>::reverse() {
    reverse(head_, tail_);
    // reverse(head_, head_->next);
    // reverse(head_->next->next, tail_->prev->prev);
    // reverse(head_->next, head_->next->next);
    // reverse(tail_->prev->prev, tail_->prev);
}

/**
 * Helper function to reverse a sequence of linked memory inside a List,
 * starting at startPoint and ending at endPoint. You are responsible for
 * updating startPoint and endPoint to point to the new starting and ending
 * points of the rearranged sequence of linked memory in question.
 *
 * @param startPoint A pointer reference to the first node in the sequence
 *  to be reversed.
 * @param endPoint A pointer reference to the last node in the sequence to
 *  be reversed.
 */
template <class T>
void List<T>::reverse(ListNode *& startPoint, ListNode *& endPoint) {
    /// @TODO Graded in MP3.1

    // if either end is NULL, we cant reverse them
    if (!startPoint || !endPoint) {
        return;
    }

    // if the start and end points are the same,
    // reversing will do nothing
    if (startPoint == endPoint) {
        return;
    }

    ListNode* before = startPoint->prev;
    ListNode* current = startPoint;
    ListNode* after = endPoint->next;
    while(current != endPoint) {
        ListNode* next = current->next;
        current->next = current->prev;
        current->prev = next;
        current = next;
    }

    // startPoint->next points to the the element after end
    startPoint->next = endPoint->next;
    // reverse the the endpoint and link it to the element before
    endPoint->next = endPoint->prev;
    endPoint->prev = before;

    // swap the start and end points
    ListNode* temp = startPoint;
    startPoint = endPoint;
    endPoint = temp;

    // back-link the node after end to the end point
    if (after != NULL) {
        after->prev = endPoint;
    }
    // forward-link the node before start to the start point
    if (before != NULL) {
        before->next = startPoint;
    }

    // /******* DEBUGGING *******/
    // ListNode* node = head_;
    // std::cout << "head_ = " << head_->data << std::endl;
    // while (node != NULL) {
    //     if (node->prev == NULL) {
    //         std::cout << "NULL";
    //     } else {
    //         std::cout << "   " << node->prev->data;
    //     }
    //     std::cout << " <- " << node->data << " -> ";
    //
    //     if (node->next == NULL) {
    //         std::cout << "NULL" << std::endl;
    //     } else {
    //         std::cout << node->next->data << std::endl;
    //     }
    //
    //     node = node->next;
    // }
    // std::cout << "tail_ = " << tail_->data << std::endl;

}

/**
 * Reverses blocks of size n in the current List. You should use your
 * reverse( ListNode * &, ListNode * & ) helper function in this method!
 *
 * @param n The size of the blocks in the List to be reversed.
 */
template <class T>
void List<T>::reverseNth(int n) {
    /// @TODO Graded in MP3.1

    // Base Cases:
    //  - if the list contains 0 or 1 element, reversing does nothing; return
    //  - if the block size is smaller than 1, reversing does nothing; return
    //  - if the block size is bigger than the list, reverse from head_ to tail_
    if (length_ <= 1 || n <= 1) {
        return;
    } else if (n >= length_) {
        reverse(head_, tail_);
        return;
    }

    // calculate how many complete blocks we have to reverse
    int completeBlocks = length_ / n;
    ListNode* start = head_;
    ListNode* end = start;
    for (int k = 0; k < completeBlocks; k ++) {
        // advance 'end' n times to point to the end of the block
        for (int i = 1; i < n; i ++) {
            end = end->next;
        }

        // call reverse, but substitute head_ or tail_ when
        // necessary to make sure they are updated properly
        if (start == head_)     reverse(head_, end);
        else if (end == tail_)  reverse(start, tail_);
        else                    reverse(start, end);

        // update start and end to the first node after this block
        start = end->next;
        end = start;
    }

    // if there are any leftover elements, swap them
    if (length_ % n != 0) reverse(start, tail_);


    // /******* DEBUGGING *******/
    // ListNode* node = head_;
    // std::cout << "head_ = " << head_->data << std::endl;
    // while (node != NULL) {
    //     if (node->prev == NULL) {
    //         std::cout << "NULL";
    //     } else {
    //         std::cout << "   " << node->prev->data;
    //     }
    //     std::cout << " <- " << node->data << " -> ";
    //
    //     if (node->next == NULL) {
    //         std::cout << "NULL" << std::endl;
    //     } else {
    //         std::cout << node->next->data << std::endl;
    //     }
    //
    //     node = node->next;
    // }
    // std::cout << "tail_ = " << tail_->data << std::endl;
}


/**
 * Modifies the List using the waterfall algorithm.
 * Every other node (starting from the second one) is removed from the
 * List, but appended at the back, becoming the new tail. This continues
 * until the next thing to be removed is either the tail (**not necessarily
 * the original tail!**) or NULL.  You may **NOT** allocate new ListNodes.
 * Note that since the tail should be continuously updated, some nodes will
 * be moved more than once.
 */
template <class T>
void List<T>::waterfall() {
    /// @TODO Graded in MP3.1

    // if the length is 0, we can't do a waterfall
    // if the length is 1, the waterfall is trivial
    if (length_ <= 1) {
        return;
    }

    // start at the second node because we are skipping the first
    ListNode* curr = head_->next;
    while (curr != tail_ && curr != NULL) {
        // repair the hole created by removing the current node
        ListNode* prev = curr->prev;
        ListNode* next = curr->next;
        prev->next = next;
        if (next) {
            next->prev = prev;
        }

        // attach the current node after the tail and
        // set the next pointer to NULL
        tail_->next = curr;
        curr->prev = tail_;
        curr->next = NULL;

        // update tail_ to point to the current node
        tail_ = curr;

        // advance two spaces using prev as a standin
        // for the previous location of curr
        curr = prev->next->next;
    }
}

/**
 * Splits the given list into two parts by dividing it at the splitPoint.
 *
 * @param splitPoint Point at which the list should be split into two.
 * @return The second list created from the split.
 */
template <class T>
List<T> List<T>::split(int splitPoint) {
    if (splitPoint > length_)
        return List<T>();

    if (splitPoint < 0)
        splitPoint = 0;

    ListNode * secondHead = split(head_, splitPoint);

    int oldLength = length_;
    if (secondHead == head_) {
        // current list is going to be empty
        head_ = NULL;
        tail_ = NULL;
        length_ = 0;
    } else {
        // set up current list
        tail_ = head_;
        while (tail_ -> next != NULL)
            tail_ = tail_->next;
        length_ = splitPoint;
    }

    // set up the returned list
    List<T> ret;
    ret.head_ = secondHead;
    ret.tail_ = secondHead;
    if (ret.tail_ != NULL) {
        while (ret.tail_->next != NULL)
            ret.tail_ = ret.tail_->next;
    }
    ret.length_ = oldLength - splitPoint;
    return ret;
}

/**
 * Helper function to split a sequence of linked memory at the node
 * splitPoint steps **after** start. In other words, it should disconnect
 * the sequence of linked memory after the given number of nodes, and
 * return a pointer to the starting node of the new sequence of linked
 * memory.
 *
 * This function **SHOULD NOT** create **ANY** new List or ListNode objects!
 *
 * @param start The node to start from.
 * @param splitPoint The number of steps to walk before splitting.
 * @return The starting node of the sequence that was split off.
 */
template <class T>
typename List<T>::ListNode * List<T>::split(ListNode * start, int splitPoint) {
    /// @TODO Graded in MP3.2

    // step 'splitPoint' times through the list
    for (int i = 0; i < splitPoint; i ++) {
        start = start->next;
    }

    // save the previous node
    ListNode* prev = start->prev;
    // disconnect the lists at startPoint
    start->prev = NULL;
    prev->next = NULL;

    return start;
}

/**
 * Merges the given sorted list into the current sorted list.
 *
 * @param otherList List to be merged into the current list.
 */
template <class T>
void List<T>::mergeWith(List<T> & otherList) {
    // set up the current list
    head_ = merge(head_, otherList.head_);
    tail_ = head_;

    // make sure there is a node in the new list
    if (tail_ != NULL) {
        while (tail_->next != NULL)
            tail_ = tail_->next;
    }
    length_ = length_ + otherList.length_;

    // empty out the parameter list
    otherList.head_ = NULL;
    otherList.tail_ = NULL;
    otherList.length_ = 0;
}

/**
 * Helper function to merge two **sorted** and **independent** sequences of
 * linked memory. The result should be a single sequence that is itself
 * sorted.
 *
 * This function **SHOULD NOT** create **ANY** new List objects.
 *
 * @param first The starting node of the first sequence.
 * @param second The starting node of the second sequence.
 * @return The starting node of the resulting, sorted sequence.
 */
template <class T>
typename List<T>::ListNode * List<T>::merge(ListNode * first, ListNode* second) {
    /// @TODO Graded in MP3.2

    // if either list is empty,
    // we can simply return the other
    if (first == NULL) return second;
    else if (second == NULL) return first;


    // rename the lists so the one with the smaller
    // data at the front is called 'first'
    if (second->data < first->data) {
        ListNode* swap = first;
        first = second;
        second = swap;
    }
    // first now holds the smaller of the two
    // list heads.  This will be the head of the merged list
    ListNode* result = first;

    while (second != NULL) {
        // store the next element in list 1
        ListNode * temp = first->next;

        // as long as there are more elements
        // in the first list, compare them
        if (temp != NULL) {
            // if list 2 is smaller than list 1...
            if (second->data < temp->data) {
                // forward link the first list to the second
                // and back link the second list to the first
                first->next = second;
                second->prev = first;

                // then rename the second list as the first
                // and the saved next element as the second
                // (essentially swapping the two lists without)
                // the node we just compared)
                first = second;
                second = temp;
            }
            else {
                // if list 1 is still smaller, then simply advance
                // along list 1 until we find a spot for list 2
                first = temp;
            }
        }
        // if we made it to the end of a list,
        // simply link the second list and be done.
        else {
            first->next = second;
            second->prev = first;
            break;
        }
    }

    // return the pointer to the merged list
    return result;
}

/**
 * Sorts the current list by applying the Mergesort algorithm.
 */
template <class T>
void List<T>::sort() {
    if (empty())
        return;
    head_ = mergesort(head_, length_);
    tail_ = head_;
    while (tail_->next != NULL)
        tail_ = tail_->next;
}

/**
 * Sorts a chain of linked memory given a start node and a size.
 * This is the recursive helper for the Mergesort algorithm (i.e., this is
 * the divide-and-conquer step).
 *
 * @param start Starting point of the chain.
 * @param chainLength Size of the chain to be sorted.
 * @return A pointer to the beginning of the now sorted chain.
 */
template <class T>
typename List<T>::ListNode* List<T>::mergesort(ListNode * start, int chainLength) {
    /// @TODO Graded in MP3.2

    // /******* DEBUGGING *******/
    // ListNode* node = start;
    // std::cout << "-->" << std::endl;
    // while (node != NULL) {
    //     if (node->prev == NULL) {
    //         std::cout << "NULL";
    //     } else {
    //         std::cout << "   " << node->prev->data;
    //     }
    //     std::cout << " <- " << node->data << " -> ";
    //
    //     if (node->next == NULL) {
    //         std::cout << "NULL" << std::endl;
    //     } else {
    //         std::cout << node->next->data << std::endl;
    //     }
    //
    //     node = node->next;
    // }
    // std::cout << std::endl;

    // base case:
    if (start == NULL) return NULL;
    if (chainLength == 1) return start;
    // std::cout << __LINE__ << std::endl;

    ListNode* first = start;
    ListNode* second = split(start, (int)chainLength/2);


    // By adding 1 to the chainLength for the second mergesort before dividing,
    // we are essentially adding 0.5 before truncating the number to an int.
    // For even chainLength, this has no effect because cL/2 is an integer,
    // so adding 0.5 before truncating yields the same result.
    // For odd chainLength, the first mergesort will undershoot by 0.5,
    // so adding the 0.5 to the second one means we get the remainder of uneven lists
    first = mergesort(first, chainLength/2);
    second = mergesort(second, (chainLength+1)/2);

    start = merge(first, second);

    // /******* DEBUGGING *******/
    // node = start;
    // std::cout << "<--" << std::endl;
    // while (node != NULL) {
    //     if (node->prev == NULL) {
    //         std::cout << "NULL";
    //     } else {
    //         std::cout << "   " << node->prev->data;
    //     }
    //     std::cout << " <- " << node->data << " -> ";
    //
    //     if (node->next == NULL) {
    //         std::cout << "NULL" << std::endl;
    //     } else {
    //         std::cout << node->next->data << std::endl;
    //     }
    //
    //     node = node->next;
    // }
    // std::cout << std::endl;

    return start;
}
