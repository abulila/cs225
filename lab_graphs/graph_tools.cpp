/**
 * @file graph_tools.cpp
 * This is where you will implement several functions that operate on graphs.
 * Be sure to thoroughly read the comments above each function, as they give
 *  hints and instructions on how to solve the problems.
 */

#include "graph_tools.h"
#include <iostream>
/**
 * Finds the minimum edge weight in the Graph graph.
 * THIS FUNCTION IS GRADED.
 *
 * @param graph - the graph to search
 * @return the minimum weighted edge
 *
 * @todo Label the minimum edge as "MIN". It will appear blue when
 *  graph.savePNG() is called in minweight_test.
 *
 * @note You must do a traversal.
 * @note You may use the STL stack and queue.
 * @note You may assume the graph is connected.
 *
 * @hint Initially label vertices and edges as unvisited.
 */
int GraphTools::findMinWeight(Graph& graph)
{
	// initially set every vertex and edge to UNEXPLORED
	vector<Vertex> vertices = graph.getVertices();
	vector<Edge> edges = graph.getEdges();
	for (Vertex v : vertices) {
		graph.setVertexLabel(v, "UNEXPLORED");
	}
	for (Edge e : edges) {
		graph.setEdgeLabel(e.source, e.dest, "UNEXPLORED");
	}

	// do a BFS to label each edge and find the minWeight edge
	Edge minEdge = edges[0];
	for (Vertex v : vertices) {
		if (graph.getVertexLabel(v) == "UNEXPLORED") {
			minEdge = std::min(minEdge, BFS(graph, v));
		}
	}

	graph.setEdgeLabel(minEdge.source, minEdge.dest, "MIN");
	return graph.getEdgeWeight(minEdge.source, minEdge.dest);
}

Edge GraphTools::BFS(Graph& graph, Vertex start) {
	Edge minEdge;
	std::queue<Vertex> q;
	graph.setVertexLabel(start, "VISITED");
	q.push(start);

	graph.initSnapshot("myBFS");

	while (!q.empty()) {
		graph.snapshot();
		Vertex v = q.front(); q.pop();
		for (Vertex w : graph.getAdjacent(v)) {
			// if minEdge is still the default, set it to this edge,
			// otherwise update only if this edge is lower weight
			if (minEdge.getWeight() == -1) minEdge = graph.getEdge(v, w);
			else minEdge = std::min(minEdge, graph.getEdge(v, w));

			// for each unexplored neighbor, mark a discovery edge and
			// add the neighbor to the traversal queue
			if (graph.getVertexLabel(w) == "UNEXPLORED") {
				graph.setEdgeLabel(v, w, "DISCOVERY");
				graph.setVertexLabel(w, "VISITED");
				q.push(w);
			}
			// if the neighbor is explored, but the edge is not,
			// mark a cross edge
			else if (graph.getEdgeLabel(v, w) == "UNEXPLORED") {
				graph.setEdgeLabel(v, w, "CROSS");
			}
			// if the neighbor and edge are both marked, do nothing.
		}
	}
	return minEdge;
}

/**
 * Returns the shortest distance (in edges) between the Vertices
 *  start and end.
 * THIS FUNCTION IS GRADED.
 *
 * @param graph - the graph to search
 * @param start - the vertex to start the search from
 * @param end - the vertex to find a path to
 * @return the minimum number of edges between start and end
 *
 * @todo Label each edge "MINPATH" if it is part of the minimum path
 *
 * @note Remember this is the shortest path in terms of edges,
 *  not edge weights.
 * @note Again, you may use the STL stack and queue.
 * @note You may also use the STL's unordered_map, but it is possible
 *  to solve this problem without it.
 *
 * @hint In order to draw (and correctly count) the edges between two
 *  vertices, you'll have to remember each vertex's parent somehow.
 */
int GraphTools::findShortestPath(Graph& graph, Vertex start, Vertex end)
{
	// initially set every vertex and edge to UNEXPLORED
	vector<Vertex> vertices = graph.getVertices();
	vector<Edge> edges = graph.getEdges();
	for (Vertex v : vertices) {
		graph.setVertexLabel(v, "UNEXPLORED");
	}
	for (Edge e : edges) {
		graph.setEdgeLabel(e.source, e.dest, "UNEXPLORED");
	}

	// maps for each vertex's distance and previous node
    std::unordered_map<Vertex, int> dist;
	std::unordered_map<Vertex, Vertex> prev;

	// start by setting the distance of the start node to 0
	dist[start] = 0;

	std::queue<Vertex> q;
	graph.setVertexLabel(start, "VISITED");
	q.push(start);

	// perform a modified BFS to track the distance and parent
	// of each node instead of discovery and cross edges
	while (!q.empty()) {
		Vertex v = q.front(); q.pop();
		for (Vertex w : graph.getAdjacent(v)) {
			// for each unexplored neighbor, record the distance and
			// parent, then mark the node as visited.
			if (graph.getVertexLabel(w) == "UNEXPLORED") {
				dist[w] = dist[v] + 1;
				prev[w] = v;
				graph.setVertexLabel(w, "VISITED");
				q.push(w);
			}
		}
	}

	// use the prev[] array to backtrace the minimal path
	// from end to start
	Vertex v = end;
	while (prev[v] != Vertex()) {
		graph.setEdgeLabel(prev[v], v, "MINPATH");
		v = prev[v];
	}

	graph.initSnapshot("findMinPath");
	graph.snapshot();

    return dist[end];
}

/**
 * Finds a minimal spanning tree on a graph.
 * THIS FUNCTION IS GRADED.
 *
 * @param graph - the graph to find the MST of
 *
 * @todo Label the edges of a minimal spanning tree as "MST"
 *  in the graph. They will appear blue when graph.savePNG() is called.
 *
 * @note Use your disjoint sets class from MP 7.1 to help you with
 *  Kruskal's algorithm. Copy the files into the dsets.h and dsets.cpp .
 * @note You may call std::sort instead of creating a priority queue.
 */
void GraphTools::findMST(Graph& graph)
{
	// get the list of vertices and edges in the graph
	// and sort the edge list by increasing weight
	vector<Vertex> vertices = graph.getVertices();
    vector<Edge> edges = graph.getEdges();
	std::sort(edges.begin(), edges.end());

	// create a disjoint set for each vertex
	DisjointSets MST;
	MST.addelements(vertices.size());

	int MST_edges = 0;

	// for each edge in the sorted list, check if the connected
	// vertices are in the MST, and add them if not.
	for (Edge e : edges) {

		// the DisjointSet is based on index, not key, so we must
		// find the indecies of the source and dest vertices
		int sourceIndex = -1;
		int destIndex = -1;
		for (size_t i = 0; i < vertices.size(); i ++) {
			if (vertices[i] == e.source) sourceIndex = i;
			if (vertices[i] == e.dest)   destIndex = i;
			// once we found both, we don't need to check the rest
			if (sourceIndex >= 0 && destIndex >= 0) break;
		}

		// check if the edge connects vertices in different sets
		if (MST.find(sourceIndex) != MST.find(destIndex)) {
			// add the edge to the MST and union the vertices' sets
			graph.setEdgeLabel(e.source, e.dest, "MST");
			MST.setunion(sourceIndex, destIndex);
			MST_edges ++;
		}
		else {
			graph.setEdgeLabel(e.source, e.dest, "UNEXPLORED");
		}

		// stop checking edges when we have
		if (MST_edges >= vertices.size() - 1) break;
	}
}
