/**
 * @file DFS.h
 */

#ifndef DFS_H
#define DFS_H

#include <iterator>
#include <cmath>
#include <vector>
#include <stack>

#include "../cs225/PNG.h"
#include "../Point.h"

#include "ImageTraversal.h"

using namespace cs225;

/**
 * A depth-first ImageTraversal.
 * Derived from base class ImageTraversal
 */
class DFS : public ImageTraversal {
public:
    DFS(const PNG & png, const Point & start, double tolerance);
    virtual ~DFS();

    ImageTraversal::Iterator begin();
    ImageTraversal::Iterator end();

    void add(const Point & point);
    Point pop();
    Point peek() const;
    bool empty() const;

private:
	/** @todo [Part 1] */
	/** add private members here*/
    PNG png;
    Point start;
    double tolerance;
    stack<Point> stack;     // queue of points to traverse
    vector<bool> visited;   // 2D array (stored as 1D) indicating whether
                            // the corresponding point has been visited
    DFS* heapMemoryTraversal = NULL;

    bool _isVisited(Point p);
    void _markVisited(Point p);
    bool _isValid(Point p);
};

#endif
