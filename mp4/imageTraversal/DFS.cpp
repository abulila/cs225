#include <iterator>
#include <cmath>
#include <list>
#include <stack>
#include <climits>

#include "../cs225/PNG.h"
#include "../Point.h"

#include "ImageTraversal.h"
#include "DFS.h"

/**
 * Initializes a depth-first ImageTraversal on a given `png` image,
 * starting at `start`, and with a given `tolerance`.
 * @param png The image this DFS is going to traverse
 * @param start The start point of this DFS
 * @param tolerance If the current point is too different (difference larger than tolerance) with the start point,
 * it will not be included in this DFS
 */
DFS::DFS(const PNG & png, const Point & start, double tolerance)
    : png(png), start(start), tolerance(tolerance)
{
    /** @todo [Part 1] */
    stack.push(start);
    visited.resize(png.width() * png.height(), false);
}

DFS::~DFS() {
    if (heapMemoryTraversal != NULL) {
        delete heapMemoryTraversal;
        heapMemoryTraversal = NULL;
    }
}

/**
 * Returns an iterator for the traversal starting at the first point.
 */
ImageTraversal::Iterator DFS::begin() {
    /** @todo [Part 1] */
    if (heapMemoryTraversal != NULL) {
        delete heapMemoryTraversal;
    }

    DFS* dfs = new DFS(png, start, tolerance);
    heapMemoryTraversal = dfs;
    return ImageTraversal::Iterator(*dfs);
}

/**
 * Returns an iterator for the traversal one past the end of the traversal.
 */
ImageTraversal::Iterator DFS::end() {
    /** @todo [Part 1] */
    return ImageTraversal::Iterator();
}

/**
 * Adds a Point for the traversal to visit at some point in the future.
 */
void DFS::add(const Point & point) {
    /** @todo [Part 1] */
    unsigned x = point.x;
    unsigned y = point.y;

    Point right(x+1, y);
    Point above(x, y-1);
    Point left (x-1, y);
    Point below(x, y+1);

    // push point to the right
    if (_isValid(right)) {
        stack.push(right);
    }
    // push point below
    if (_isValid(below)) {
        stack.push(below);
    }
    // push point to the left
    if (_isValid(left)) {
        stack.push(left);
    }
    // push point above
    if (_isValid(above)) {
        stack.push(above);
    }
}

/**
 * Removes and returns the current Point in the traversal.
 */
Point DFS::pop() {
    /** @todo [Part 1] */
    Point retPoint = peek();            // grab the top point from the stack
    Point p = retPoint;                 // starting with the top...
    _markVisited(p);                    // ...mark as visited

    while(!empty() && _isVisited(p)) {  // ... and for every visited point on
        stack.pop();                    // the top of the stack, remove it
        p = peek();
    }                                   // the stack is now "clean"

    return retPoint;                    // return the original value we peeked.
}

/**
 * Returns the current Point in the traversal.
 */
Point DFS::peek() const {
    /** @todo [Part 1] */
    return empty() ? Point(UINT_MAX,UINT_MAX) : stack.top();
}

/**
 * Returns true if the traversal is empty.
 */
bool DFS::empty() const {
    /** @todo [Part 1] */
    return stack.empty();
}


bool DFS::_isVisited(Point p) {
    if (p.x < png.width() && p.y < png.height()) {
        return visited[(p.y * png.width()) + p.x];
    }
    // if the point is out of bounds,
    // return true so we ignore it
    return true;
}


void DFS::_markVisited(Point p) {
    if (p.x < png.width() && p.y < png.height()) {
        visited[(p.y * png.width()) + p.x] = true;
    }
}


bool DFS::_isValid(Point p) {
    // check bounds
    if (p.x >= png.width() || p.y >= png.height())
        return false;

    // check visited
    if (_isVisited(p))
        return false;

    // check tolerance
    cs225::HSLAPixel &p1 = png.getPixel(p.x, p.y);
    cs225::HSLAPixel &p2 = png.getPixel(start.x, start.y);

    return (calculateDelta(p1, p2) <= tolerance);
}
