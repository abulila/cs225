#include <iterator>
#include <cmath>
#include <vector>
#include <queue>

#include "../cs225/PNG.h"
#include "../Point.h"

#include "ImageTraversal.h"
#include "BFS.h"

/**
 * Initializes a breadth-first ImageTraversal on a given `png` image,
 * starting at `start`, and with a given `tolerance`.
 * @param png The image this BFS is going to traverse
 * @param start The start point of this BFS
 * @param tolerance If the current point is too different (difference larger than tolerance) with the start point,
 * it will not be included in this BFS
 */
BFS::BFS(const PNG & png, const Point & start, double tolerance)
: png(png), start(start), tolerance(tolerance)
{
    /** @todo [Part 1] */
    queue.push(start);
    visited.resize(png.width() * png.height(), false);
    visited[(start.y * png.width()) + start.x] = true;
}

BFS::~BFS() {
    if (heapMemoryTraversal != NULL) {
        delete heapMemoryTraversal;
        heapMemoryTraversal = NULL;
    }
}


/**
 * Returns an iterator for the traversal starting at the first point.
 */
ImageTraversal::Iterator BFS::begin() {
    /** @todo [Part 1] */
    if (heapMemoryTraversal != NULL) {
        delete heapMemoryTraversal;
    }

    BFS* bfs = new BFS(png, start, tolerance);
    heapMemoryTraversal = bfs;
    return ImageTraversal::Iterator(*bfs);
}

/**
 * Returns an iterator for the traversal one past the end of the traversal.
 */
ImageTraversal::Iterator BFS::end() {
    /** @todo [Part 1] */
    return ImageTraversal::Iterator();
}

/**
 * Adds a Point for the traversal to visit at some point in the future.
 */
void BFS::add(const Point & point) {
    /** @todo [Part 1] */
    unsigned x = point.x;
    unsigned y = point.y;

    Point below(x, y+1);
    Point above(x, y-1);
    Point left (x-1, y);
    Point right(x+1, y);

    // push point to the right
    if (_isValid(right)) {
        queue.push(right);
        visited[(right.y * png.width()) + right.x] = true;
    }
    // push point below
    if (_isValid(below)) {
        queue.push(below);
        visited[(below.y * png.width()) + below.x] = true;
    }
    // push point to the left
    if (_isValid(left)) {
        queue.push(left);
        visited[(left.y * png.width()) + left.x] = true;
    }
    // push point above
    if (_isValid(above)) {
        queue.push(above);
        visited[(above.y * png.width()) + above.x] = true;
    }
}

/**
 * Removes and returns the current Point in the traversal.
 */
Point BFS::pop() {
    /** @todo [Part 1] */
    Point p = queue.front();
    queue.pop();
    return p;
}

/**
 * Returns the current Point in the traversal.
 */
Point BFS::peek() const {
    /** @todo [Part 1] */
    return empty() ? Point(0,0) : queue.front();
}

/**
 * Returns true if the traversal is empty.
 */
bool BFS::empty() const {
    /** @todo [Part 1] */
    return queue.empty();
}

bool BFS::_isValid(Point p) {
    // check bounds
    if (p.x >= png.width() || p.y >= png.height())
        return false;

    // check visited
    if (visited[(p.y * png.width()) + p.x])
        return false;

    // check tolerance
    cs225::HSLAPixel p1 = png.getPixel(p.x, p.y);
    cs225::HSLAPixel p2 = png.getPixel(start.x, start.y);

    return (calculateDelta(p1, p2) <= tolerance);
}
