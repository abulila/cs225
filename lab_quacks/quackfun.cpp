/**
 * @file quackfun.cpp
 * This is where you will implement the required functions for the
 *  stacks and queues portion of the lab.
 */

namespace QuackFun {

/**
 * Sums items in a stack.
 * @param s A stack holding values to sum.
 * @return The sum of all the elements in the stack, leaving the original
 *  stack in the same state (unchanged).
 *
 * @note You may modify the stack as long as you restore it to its original
 *  values.
 * @note You may use only two local variables of type T in your function.
 *  Note that this function is templatized on the stack's type, so stacks of
 *  objects overloading the + operator can be summed.
 * @note We are using the Standard Template Library (STL) stack in this
 *  problem. Its pop function works a bit differently from the stack we
 *  built. Try searching for "stl stack" to learn how to use it.
 * @hint Think recursively!
 */
template <typename T>
T sum(stack<T>& s)
{
    if (s.empty()) {
        return T();
    }

    T top = s.top();                // Save the current top value
    s.pop();                        // and pop it off the stack
    T partialSum = sum(s);          // Recursively call sum() on smaller
                                    // stacks until the stack is empty
    s.push(top);                    // Push the top value back on as the
                                    // program recurses back up the function
    return partialSum + top;        // Return the running sum
}

/**
 * Checks whether the given string (stored in a queue) has balanced brackets.
 * A string will consist of
 * square bracket characters, [, ], and other characters. This function will return
 * true if and only if the square bracket characters in the given
 * string are balanced. For this to be true,
 * all brackets must be matched up correctly, with no extra, hanging, or unmatched
 * brackets. For example, the string "[hello][]" is balanced, "[[][[]a]]" is balanced,
 * "[]]" is unbalanced, "][" is unbalanced, and "))))[cs225]" is balanced.
 *
 * You are allowed only the use of one stack in this function, and no other local variables.
 *
 * @param input The queue representation of a string to check for balanced brackets in
 * @return Whether the input string had balanced brackets
 */
bool isBalanced(queue<char> input)
{
    stack<char> s;

    while (!input.empty()) {
        if (input.front() == '[') {
            s.push('[');
        }
        else if (input.front() == ']') {
            // if there is no matching opening bracket
            // to pop, return false
            if (s.empty())
                return false;
            // otherwise pop the matching bracket
            s.pop();
        }
    }

    return s.empty();
}

/**
 * Reverses even sized blocks of items in the queue. Blocks start at size
 * one and increase for each subsequent block.
 * @param q A queue of items to be scrambled
 *
 * @note Any "leftover" numbers should be handled as if their block was
 *  complete.
 * @note We are using the Standard Template Library (STL) queue in this
 *  problem. Its pop function works a bit differently from the stack we
 *  built. Try searching for "stl stack" to learn how to use it.
 * @hint You'll want to make a local stack variable.
 */
template <typename T>
void scramble(queue<T>& q)
{
    stack<T> s;
    queue<T> q2;

    int n = 1;
    int count = 0;
    while (!q.empty()) {
        // if we completed a group, increase n and reset count
        if (count == n) {
            n ++;
            count = 0;
        }

        // if n is odd, this group should remain in order
        if (n % 2 == 1) {
            q2.push(q.front()); // store the value in the temp Q
            q.pop();            // and take it off the input Q
            count ++;
        }
        else {
            // push the input elements onto a stack, effectively reversing them
            s.push(q.front());
            q.pop();
            count ++;
            // once we've pushed the whole group, pop each element
            // off the stack back into the queue
            if (count == n || q.empty()) {
                while (!s.empty()) {
                    q2.push(s.top());
                    s.pop();
                }
            }
        }
    }

    // copy the temporary q2 back into the input, q
    while (!q2.empty()) {
        q.push(q2.front());
        q2.pop();
    }
}

/**
 * @return true if the parameter stack and queue contain only elements of
 *  exactly the same values in exactly the same order; false, otherwise.
 *
 * @note You may assume the stack and queue contain the same number of items!
 * @note The back of the queue corresponds to the top of the stack!
 * @note There are restrictions for writing this function.
 * - Your function may not use any loops
 * - In your function you may only declare ONE local boolean variable to use in
 *   your return statement, and you may only declare TWO local variables of
 *   parametrized type T to use however you wish.
 * - No other local variables can be used.
 * - After execution of verifySame, the stack and queue must be unchanged. Be
 *   sure to comment your code VERY well.
 */
template <typename T>
bool verifySame(stack<T>& s, queue<T>& q)
{
    /* Proceedure: recurse down to the bottom of the stack, then
     * compare each element to the front of the queue at each step
     * on the way back up.  After each step, we rotate the queue so
     * the second to last element in the stack compares against the
     * second element in the queue, etc.  After the final recursive
     * step, the queue will have rotated all the way back.
     */

    // base case: the stack is empty, which means we hit the bottom of
    // stack on the previous recursive step, so return assume equality
    // and go back up.
    if (s.empty()) return true;

    // save the value on the stack so we can reconstruct it later,
    // then pop it off so we can recurse down to the bottom of the stack
    T top = s.top();
    s.pop();

    // recursion: perform the same proceedure on the stack without the
    // top element, but without changing the queue.
    bool same = verifySame(s, q);

    // restore the stack element after we come back out of the recusion
    // Note: after returning from the base case, the bottom element of
    // the stack is restored here so it is compared with the (unmodified)
    // front of the queue.
    s.push(top);

    // save the front of the queue so we can move it to the back
    T front = q.front();
    // pop and push the queue to rotate the front to the back
    q.pop();
    q.push(front);

    // if the top of the stack (at this step of the recursion)
    // is the same as the front of the queue, and it has been
    // for every earlier step in the recursion, then return true
    return (top == front) && same;
}

} // end namespace QuackFun
