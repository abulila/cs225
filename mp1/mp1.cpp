#include "mp1.h"
#include "cs225/PNG.h"
#include "cs225/HSLAPixel.h"
#include <string>
#include <cmath>

using namespace cs225;

typedef struct {
    double h;  // angle in degrees, [0, 360]
    double s;  // [0, 1]
    double l;  // [0, 1]
    double a;  // [0, 1]
} hslaColor;

hslaColor mandelbrotHue(double x0, double y0);

void rotate(std::string inputFile, std::string outputFile) {
    PNG image;

    image.readFromFile(inputFile);
    unsigned width = image.width();
    unsigned height = image.height();

    for (unsigned x = 0; x < width; x++) {
        for (unsigned y = 0; y < height / 2; y++) {

            HSLAPixel * pixel1 = image.getPixel(x, y);
            HSLAPixel * pixel2 = image.getPixel(width - x - 1, height - y - 1);

            std::swap(pixel1->h, pixel2->h);
            std::swap(pixel1->s, pixel2->s);
            std::swap(pixel1->l, pixel2->l);
            std::swap(pixel1->a, pixel2->a);
        }
    }

    if (height % 2 == 1) {
        int midY = height / 2;
        for (unsigned x = 0; x < width / 2; x++) {
            HSLAPixel * pixel1 = image.getPixel(x, midY);
            HSLAPixel * pixel2 = image.getPixel(width - x - 1, midY);

            std::swap(pixel1->h, pixel2->h);
            std::swap(pixel1->s, pixel2->s);
            std::swap(pixel1->l, pixel2->l);
            std::swap(pixel1->a, pixel2->a);
        }
    }

    image.writeToFile(outputFile);
}

PNG myArt(unsigned int width, unsigned int height) {
    PNG png(width, height);

    for (unsigned x = 0; x < png.width(); x++) {
        for (unsigned y = 0; y < png.height(); y++) {
            HSLAPixel * pixel = png.getPixel(x,y);

            double scaled_x = (x * 2.0 / png.width()) - 1.5;
            double scaled_y = (y * 2.0 / png.height()) - 1;
            hslaColor pixelColor = mandelbrotHue(scaled_x,scaled_y);
            pixel->h = pixelColor.h;
            pixel->s = pixelColor.s;
            pixel->l = pixelColor.l;
            pixel->a = pixelColor.a;
        }
    }

    return png;
}


/* Helper function to compute the hue of a point in the Mandelbrot
 * set, based on the "escape time" or how many iterations are required
 * before the value of the complex coordinate are out of bounds.
 *
 * Algorithms and optimizations found on Mandelbrot Set Wikipedia page:
 *      https://en.wikipedia.org/wiki/Mandelbrot_set
 *
 * @param x0    real coordinate value in the range (-2.5, 1)
 * @param y0    imaginary coordinate value in the range (-1,1)
 * @return double representing hue of pixel in range [0,360)
 */
hslaColor mandelbrotHue(double x0, double y0) {
    hslaColor hsl;

    // before performing the escape time algorithm, check
    // if point is inside the main cardioid or the period-2
    // bulb.
    double q = ((x0 - 0.25) * (x0 - 0.25)) + (y0 * y0);
    bool isInCardioid = (q * (q + (x0 - 0.25))) < (0.25 * y0 * y0);
    bool isInBulb = (((x0 + 1) * (x0 + 1)) + (y0 * y0)) < 0.0625;
    if (isInCardioid || isInBulb) {
        hsl.h = 0.0;
        hsl.s = 1.0;
        hsl.l = 0.0;
        hsl.a = 1.0;

        return hsl;
    }

    double x = 0.0;
    double y = 0.0;
    int iteration = 0;
    int max_iteration = 70;

    while (x*x + y*y < 4 && iteration < max_iteration) {
        double x_temp = x*x - y*y + x0;
        double y_temp = (2 * x * y) + y0;
        // if this point is the same as the previous,
        // it will never diverge. This can detect many
        // of the smaller period bulbs very quickly
        if (x == x_temp && y == y_temp) {
            iteration = max_iteration;
            break;
        }
        x = x_temp;
        y = y_temp;
        iteration ++;
    }

    double unscaled_color = iteration;
    // avoid floating point issues with points inside the set
    if (iteration < max_iteration) {
        double log_zn = log(x*x + y*y) / 2;
        double nu = log(log_zn / log(2)) / log(2);
        unscaled_color = iteration + 1 - nu;
    }

    double hue = 225 - (unscaled_color * 270 / max_iteration);
    hue += (hue < 0) ? 360 : ((hue > 360) ? -360 : 0); // make sure hue is between 0 and 360
    double luminance = sqrt(0.75 * unscaled_color / max_iteration);

    hsl.h = hue;
    hsl.s = 1.0;
    hsl.l = (iteration < max_iteration) ? luminance : 0;
    hsl.a = 1.0;

    return hsl;
}
