#include "Image.h"
#include "StickerSheet.h"

int main() {
    Image grainger, wade, slide, quote, hi, pass;
    grainger.readFromFile("Stickers/grainger.png");
    wade.readFromFile("Stickers/wade.png");
    slide.readFromFile("Stickers/slide.png");
    quote.readFromFile("Stickers/quote.png");
    hi.readFromFile("Stickers/hi.png");
    pass.readFromFile("Stickers/pass.png");

    wade.scale(0.4);
    pass.scale(3);

    StickerSheet sheet(grainger, 5);
    sheet.addSticker(slide, 510,320);
    sheet.addSticker(hi, 550,350);
    sheet.addSticker(wade, 200,480);
    sheet.addSticker(quote, 250,400);
    sheet.addSticker(pass, 0, 960);

    Image output = sheet.render();
    output.writeToFile("myImage.png");

    return 0;
}
