/**
 * @file kdtree.cpp
 * Implementation of KDTree class.
 */

#include <utility>
#include <algorithm>

using namespace std;

template <int Dim>
bool KDTree<Dim>::smallerDimVal(const Point<Dim>& first,
                                const Point<Dim>& second, int curDim) const
{
    if (first[curDim] == second[curDim]) {
        return first < second;
    }
    return first[curDim] < second[curDim];
}

template <int Dim>
bool KDTree<Dim>::shouldReplace(const Point<Dim>& target,
                                const Point<Dim>& currentBest,
                                const Point<Dim>& potential) const
{
    double curSum = 0;
    double potSum = 0;
    for (unsigned i = 0; i < Dim; i++) {
        curSum += ((target[i]-currentBest[i])*(target[i]-currentBest[i]));
        potSum += ((target[i]-potential[i])*(target[i]-potential[i]));
    }
    if (curSum == potSum) {
        return potential < currentBest;
    }
    return potSum < curSum;
}

template <int Dim>
KDTree<Dim>::KDTree(const vector<Point<Dim>>& newPoints)
{
    if (newPoints.empty()) {
        root = NULL;
        size = 0;
        return;
    }
    vector<Point<Dim>> points = newPoints;
    int median_idx = (points.size() - 1) / 2;
    select(points, 0, newPoints.size() - 1, median_idx, 0);
    root = new KDTreeNode(points[median_idx]);
    size = 1;
    root->left = createNode(points, 0, median_idx - 1, 1 % Dim);
    root->right = createNode(points, median_idx + 1, newPoints.size() - 1, 1 % Dim);
}

template <int Dim>
void KDTree<Dim>::select(vector<Point<Dim>>& list, int left, int right, int k, int curDim)
{
    if (left >= right) {
        return;
    }
    int pivotIndex = right;
    //Fuction partition:
    Point<Dim> pivotValue = list[pivotIndex]; //Pivot is already rightmost value
    int storeIndex = left;
    for (int i = left; i < right; i++) {
        if (list[i][curDim] < pivotValue[curDim]) {
            swap(list[storeIndex], list[i]);
            storeIndex++;
        } else if (list[i][curDim] == pivotValue[curDim] && list[i] < pivotValue) {
            swap(list[storeIndex], list[i]);
            storeIndex++;
        }
    }
    swap(list[pivotIndex], list[storeIndex]);
    pivotIndex = storeIndex;
    if (k == pivotIndex) {
        return;
    } else if (k < pivotIndex) {
        select(list, left, pivotIndex - 1, k, curDim);
    } else {
        select(list, pivotIndex + 1, right, k, curDim);
    }
}

template <int Dim>
typename KDTree<Dim>::KDTreeNode* KDTree<Dim>::createNode(vector<Point<Dim>>& list, int left, int right, int curDim)
{
    if (left > right) {
        return NULL;
    }
    int median_idx = (left + right) / 2;
    select(list, left, right, median_idx, curDim);
    KDTreeNode* current = new KDTreeNode(list[median_idx]);
    current->left = createNode(list, left, median_idx - 1, (curDim + 1) % Dim);
    current->right = createNode(list, median_idx + 1, right, (curDim + 1) % Dim);
    size++;
    return current;
}

template <int Dim>
KDTree<Dim>::KDTree(const KDTree& other)
{
    this->root = NULL;
    this->size = 0;
    if (other.root) {
        this->root = new KDTreeNode((other.root)->point);
        this->size = other.size;
        this->root->left = copy((other.root)->left);
        this->root->right = copy((other.root)->right);
    }
}

template <int Dim>
const KDTree<Dim>& KDTree<Dim>::operator=(const KDTree& rhs) {
    if (*this != rhs) {
        if (root) {
            clear(root->left);
            clear(root->right);
            delete root; root = NULL;
        }
        size = 0;
        if (rhs.root) {
            this->root = new KDTreeNode((rhs.root)->point);
            this->size = rhs.size;
            this->root->left = copy((rhs.root)->left);
            this->root->right = copy((rhs.root)->right);
        }
    }
}

template <int Dim>
KDTree<Dim>::~KDTree() {
    if (root) {
        clear(root->left);
        clear(root->right);
        delete root; root = NULL;
    }
    size = 0;
}

template <int Dim>
typename KDTree<Dim>::KDTreeNode* KDTree<Dim>::copy(const KDTreeNode* other) {
    if (!other) {
        return NULL;
    }
    KDTreeNode* current = new KDTreeNode(other->point);
    current->left = copy(other->left);
    current->right = copy(other->right);
    return current;
}

template <int Dim>
void KDTree<Dim>::clear(KDTreeNode* current) {
    if (!current) {
        return;
    }
    clear(current->left);
    clear(current->right);
    delete current; current = NULL;
}

template <int Dim>
Point<Dim> KDTree<Dim>::findNearestNeighbor(const Point<Dim>& query) const
{
    Point<Dim> nearest = this->root->point;
    findBest(this->root, 0, query, nearest);

    return nearest;
}

template <int Dim>
void KDTree<Dim>::findBest(const KDTreeNode* subroot, int curDim, const Point<Dim>& query, Point<Dim>& curBest) const {
    // make sure there is a node to look at
    if (subroot == NULL) {
        return;
    }
    // we hit the first leaf, so this point is the current best
    if (curBest == this->root->point && subroot->isLeaf()) {
        curBest = subroot->point;
        return;
    }

    // if the points are equal, we found the exact point,
    // so nothing will be closer and we can start returning
    if (query == subroot->point) {
        curBest = subroot->point;
        return;
    }

    // check if the query is less than the current point
    // in the current dimension.  If they are the same, use operator<
    bool checkedLeft;
    if (smallerDimVal(query, subroot->point, curDim)) {
        findBest(subroot->left, (curDim+1) % Dim, query, curBest);
        checkedLeft = true;
    } else {
        findBest(subroot->right, (curDim+1) % Dim, query, curBest);
        checkedLeft = false;
    }

    // at this point we are on our way back up the recursion, so we must check
    // the nearby hyperrectangles and recurse down if within the radius

    // check the parent node (this) against the curBest
    if (shouldReplace(query, curBest, subroot->point)) {
        curBest = subroot->point;
    }

    // check if the current spliting plane is within the radius,
    // and recurse down the other side if it is

    // create a point that is on the splitting plane and orthagonal to
    // the query to find the distance to the splitting plane
    Point<Dim> split = query;
    split[curDim] = subroot->point[curDim];
    // check if the splitting plane is closer than the current best
    if (shouldReplace(query, curBest, split)) {
        if (checkedLeft) {
            findBest(subroot->right, (curDim+1) % Dim, query, curBest);
        } else {
            findBest(subroot->left, (curDim+1) % Dim, query, curBest);
        }
        // this fixes an edge case where the root is actually the nearest neighbor,
        // but it still has to recurse down to a leaf on the other side
        if (shouldReplace(query, curBest, subroot->point)) {
            curBest = subroot->point;
        }
    }
}
