#include <iostream>
#include "dsets.h"
#include "maze.h"
#include "cs225/PNG.h"

using namespace std;

int main()
{
	// SquareMaze squaremaze;
	// squaremaze.makeMaze(5, 5);
	// squaremaze.drawMaze()->writeToFile("square.png");
	// squaremaze.drawMazeWithSolution()->writeToFile("square-soln.png");

	HexMaze hexmaze;
	hexmaze.makeMaze(30,30);
	hexmaze.drawMaze()->writeToFile("creative.png");
	hexmaze.drawMazeWithSolution()->writeToFile("creative-soln.png");

	return 0;
}
