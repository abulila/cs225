#include <vector>
#include "dsets.h"

void DisjointSets::addelements(int num) {
	for (int i = 0; i < num; i++) {
		sets.push_back(-1);
	}
}

int DisjointSets::find(int elem) {
	if (sets[elem] < 0) return elem;
	else sets[elem] = find(sets[elem]);
	return sets[elem];
}

void DisjointSets::setunion(int a, int b) {
	int rootA = find(a);
	int rootB = find(b);

	if (rootA != rootB) {
		int newSize = sets[rootA] + sets[rootB];

		if (sets[rootA] <= sets[rootB]) {
			sets[rootB] = rootA;
			sets[rootA] = newSize;
		}
		else {
			sets[rootA] = rootB;
			sets[rootB] = newSize;
		}
	}
}

int DisjointSets::size(int elem) {
	int root = find(elem);
	return -sets[root];
}
